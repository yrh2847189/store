/*
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package test

import (
	"errors"
	"fmt"
	"time"

	acPb "chainmaker.org/chainmaker/pb-go/v2/accesscontrol"
	commonPb "chainmaker.org/chainmaker/pb-go/v2/common"
	configPb "chainmaker.org/chainmaker/pb-go/v2/config"
	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store/v2/conf"
)

//DebugStore 本实现是用于避开存储模块的内存、CPU和IO的影响的情况下，提供最模拟的基本区块写入，读取等。不能安装合约，写入状态。
type DebugStore struct {
	logger      protocol.Logger
	storeConfig *conf.StorageConfig
	dbHandle    protocol.DBHandle
}

func (d *DebugStore) GetTxWithInfo(txId string) (*commonPb.TransactionInfo, error) {
	//TODO implement me
	panic("implement me")
}

func (d *DebugStore) GetTxInfoOnly(txId string) (*commonPb.TransactionInfo, error) {
	//TODO implement me
	panic("implement me")
}

//GetTxWithRWSet return tx and it's rw set
func (d *DebugStore) GetTxWithRWSet(txId string) (*commonPb.TransactionWithRWSet, error) {
	panic("implement me")
}

//GetTxInfoWithRWSet return tx and tx info and rw set
func (d *DebugStore) GetTxInfoWithRWSet(txId string) (*commonPb.TransactionInfoWithRWSet, error) {
	panic("implement me")
}
func NewDebugStore(l protocol.Logger, config *conf.StorageConfig, db protocol.DBHandle) *DebugStore {
	return &DebugStore{
		logger:      l,
		storeConfig: config,
		dbHandle:    db,
	}
}

var errNotFound = errors.New("not found")

func (d *DebugStore) QuerySingle(contractName, sql string, values ...interface{}) (protocol.SqlRow, error) {
	d.logger.Panic("implement me")
	panic("implement me")
}

func (d *DebugStore) QueryMulti(contractName, sql string, values ...interface{}) (protocol.SqlRows, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) ExecDdlSql(contractName, sql, version string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) BeginDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetDbTransaction(txName string) (protocol.SqlDBTransaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) CommitDbTransaction(txName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) RollbackDbTransaction(txName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) CreateDatabase(contractName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) DropDatabase(contractName string) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetContractDbName(contractName string) string {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetContractByName(name string) (*commonPb.Contract, error) {
	return nil, errNotFound
}

func (d *DebugStore) GetContractBytecode(name string) ([]byte, error) {
	return nil, errNotFound
}

func (d *DebugStore) GetMemberExtraData(member *acPb.Member) (*acPb.MemberExtraData, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) InitGenesis(genesisBlock *storePb.BlockWithRWSet) error {
	//put block header
	block := genesisBlock.Block
	header, _ := block.Header.Marshal()
	err := d.dbHandle.Put(append([]byte("hash_"), block.Header.BlockHash...), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte("lastBlockHeight"), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte(fmt.Sprintf("height_%d", block.Header.BlockHeight)), header)
	if err != nil {
		return err
	}
	for _, rw := range genesisBlock.TxRWSets {
		for _, w := range rw.TxWrites {
			err = d.dbHandle.Put(append([]byte(w.ContractName+"#"), w.Key...), w.Value)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (d *DebugStore) PutBlock(block *commonPb.Block, txRWSets []*commonPb.TxRWSet) error {
	d.logger.Infof("mock put block[%d] tx count:%d", block.Header.BlockHeight, len(block.Txs))
	startTime := time.Now()
	defer func() {
		d.logger.Infof("put block[%d] cost time:%s", block.Header.BlockHeight, time.Since(startTime))
	}()
	//put block header
	header, _ := block.Header.Marshal()
	err := d.dbHandle.Put(append([]byte("hash_"), block.Header.BlockHash...), header)
	if err != nil {
		return err
	}
	err = d.dbHandle.Put([]byte("lastBlockHeight"), header)
	if err != nil {
		return err
	}
	return d.dbHandle.Put([]byte(fmt.Sprintf("height_%d", block.Header.BlockHeight)), header)
}

func (d *DebugStore) GetBlockByHash(blockHash []byte) (*commonPb.Block, error) {
	data, err := d.dbHandle.Get(append([]byte("hash_"), blockHash...))
	if err != nil {
		return nil, err
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

func (d *DebugStore) BlockExists(blockHash []byte) (bool, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetHeightByHash(blockHash []byte) (uint64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetBlockHeaderByHeight(height uint64) (*commonPb.BlockHeader, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetBlock(height uint64) (*commonPb.Block, error) {
	data, err := d.dbHandle.Get([]byte(fmt.Sprintf("height_%d", height)))
	if err != nil {
		return nil, err
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

func (d *DebugStore) GetLastConfigBlock() (*commonPb.Block, error) {
	return d.GetBlock(0)
}

func (d *DebugStore) GetLastChainConfig() (*configPb.ChainConfig, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetBlockByTx(txId string) (*commonPb.Block, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetBlockWithRWSets(height uint64) (*storePb.BlockWithRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetTx(txId string) (*commonPb.Transaction, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) TxExists(txId string) (bool, error) {
	return false, nil
}

func (d *DebugStore) GetTxHeight(txId string) (uint64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetTxConfirmedTime(txId string) (int64, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetLastBlock() (*commonPb.Block, error) {
	key := []byte("lastBlockHeight")
	data, err := d.dbHandle.Get(key)
	if err != nil || len(data) == 0 {
		return nil, errNotFound
	}
	header := &commonPb.BlockHeader{}
	err = header.Unmarshal(data)
	if err != nil {
		return nil, err
	}
	return &commonPb.Block{Header: header}, nil
}

func (d *DebugStore) ReadObject(contractName string, key []byte) ([]byte, error) {
	dbKey := append([]byte(contractName+"#"), key...)
	return d.dbHandle.Get(dbKey)
}

func (d *DebugStore) SelectObject(contractName string, startKey []byte, limit []byte) (protocol.StateIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetTxRWSet(txId string) (*commonPb.TxRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetTxRWSetsByHeight(height uint64) ([]*commonPb.TxRWSet, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetDBHandle(dbName string) protocol.DBHandle {
	return d.dbHandle
}

func (d *DebugStore) GetArchivedPivot() uint64 {
	return 0
}

func (d *DebugStore) ArchiveBlock(archiveHeight uint64) error {
	return nil
}

func (d *DebugStore) RestoreBlocks(serializedBlocks [][]byte) error {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) Close() error {
	return d.dbHandle.Close()
}

func (d *DebugStore) GetHistoryForKey(contractName string, key []byte) (protocol.KeyHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetAccountTxHistory(accountId []byte) (protocol.TxHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}

func (d *DebugStore) GetContractTxHistory(contractName string) (protocol.TxHistoryIterator, error) {
	d.logger.Panic("not implement")
	panic("implement me")
}
