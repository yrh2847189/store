/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package binlog

import storePb "chainmaker.org/chainmaker/pb-go/v2/store"

type BinLogger interface {
	Close() error
	TruncateFront(index uint64) error
	ReadLastSegSection(index uint64) (data []byte, fileName string, offset, blkLen uint64, err error)
	LastIndex() (index uint64, err error)
	Write(index uint64, data []byte) (fileName string, offset, blkLen uint64, err error)
	ReadFileSection(fiIndex *storePb.StoreInfo) ([]byte, error)
}
