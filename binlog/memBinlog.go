/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package binlog

import (
	"errors"
	"fmt"
	"strconv"

	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/protocol/v2"
)

type MemBinlog struct {
	mem  map[uint64][]byte
	last uint64
	log  protocol.Logger
}

func (l *MemBinlog) ReadFileSection(fiIndex *storePb.StoreInfo) ([]byte, error) {
	i, _ := strconv.Atoi(fiIndex.FileName)
	data, found := l.mem[uint64(i)]
	if !found {
		return nil, errors.New("not found")
	}
	return data[fiIndex.Offset : fiIndex.Offset+fiIndex.ByteLen], nil
}

func NewMemBinlog(log protocol.Logger) *MemBinlog {
	return &MemBinlog{
		mem:  make(map[uint64][]byte),
		last: 0,
		log:  log,
	}
}

func (l *MemBinlog) Close() error {
	l.mem = make(map[uint64][]byte)
	l.last = 0
	return nil
}
func (l *MemBinlog) TruncateFront(index uint64) error {
	return nil
}
func (l *MemBinlog) ReadLastSegSection(index uint64) ([]byte, string, uint64, uint64, error) {
	return l.mem[index], "", 0, 0, nil
}
func (l *MemBinlog) LastIndex() (uint64, error) {
	l.log.Debugf("get last index %d", l.last)
	return l.last, nil
}
func (l *MemBinlog) Write(index uint64, data []byte) (fileName string, offset, blkLen uint64, err error) {
	if index != l.last+1 {
		return "", 0, 0, errors.New("binlog out of order")
	}
	l.mem[index] = data
	l.last = index
	l.log.Debugf("write binlog index=%d,offset=%d,len=%d", index, 0, len(data))
	return fmt.Sprintf("%d", index), 0, uint64(len(data)), nil
}
