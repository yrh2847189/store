/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package types

import (
	"bytes"

	storePb "chainmaker.org/chainmaker/pb-go/v2/store"
	"chainmaker.org/chainmaker/store/v2/historydb"
	"chainmaker.org/chainmaker/store/v2/resultdb"
)

type HistoryIteratorImpl struct {
	contractName string
	key          []byte
	dbItr        historydb.HistoryIterator
	resultStore  resultdb.ResultDB
	blockStore   BlockGetter
}
type BlockGetter interface {
	GetTxInfoOnly(txId string) (*storePb.TransactionStoreInfo, error)
}

func NewHistoryIterator(contractName string, key []byte, dbItr historydb.HistoryIterator,
	resultStore resultdb.ResultDB, blockStore BlockGetter) *HistoryIteratorImpl {
	return &HistoryIteratorImpl{
		contractName: contractName,
		key:          key,
		dbItr:        dbItr,
		resultStore:  resultStore,
		blockStore:   blockStore,
	}
}
func (hs *HistoryIteratorImpl) Next() bool {
	return hs.dbItr.Next()
}

func (hs *HistoryIteratorImpl) Value() (*storePb.KeyModification, error) {

	txId, _ := hs.dbItr.Value()
	result := storePb.KeyModification{
		TxId:     txId.TxId,
		IsDelete: false,
	}
	rwset, _ := hs.resultStore.GetTxRWSet(txId.TxId)
	for _, wset := range rwset.TxWrites {
		if bytes.Equal(wset.Key, hs.key) && wset.ContractName == hs.contractName {
			result.Value = wset.Value
		}
	}
	if len(result.Value) == 0 {
		result.IsDelete = true
	}
	tx, err := hs.blockStore.GetTxInfoOnly(txId.TxId)
	if err != nil {
		return nil, err
	}
	result.Timestamp = tx.BlockTimestamp
	result.BlockHeight = tx.BlockHeight
	return &result, nil
}
func (hs *HistoryIteratorImpl) Release() {
	hs.dbItr.Release()
}

type TxHistoryIteratorImpl struct {
	dbItr      historydb.HistoryIterator
	blockStore BlockGetter
}

func NewTxHistoryIterator(dbItr historydb.HistoryIterator, blockStore BlockGetter) *TxHistoryIteratorImpl {
	return &TxHistoryIteratorImpl{
		dbItr:      dbItr,
		blockStore: blockStore,
	}
}
func (hs *TxHistoryIteratorImpl) Next() bool {
	return hs.dbItr.Next()
}

func (hs *TxHistoryIteratorImpl) Value() (*storePb.TxHistory, error) {
	txId, _ := hs.dbItr.Value()
	result := storePb.TxHistory{
		TxId:        txId.TxId,
		BlockHeight: txId.BlockHeight,
	}
	tx, _ := hs.blockStore.GetTxInfoOnly(txId.TxId)
	result.Timestamp = tx.BlockTimestamp
	result.BlockHash = tx.BlockHash
	return &result, nil
}
func (hs *TxHistoryIteratorImpl) Release() {
	hs.dbItr.Release()
}
