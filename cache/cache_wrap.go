/*
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 */

package cache

import (
	"chainmaker.org/chainmaker/protocol/v2"
	"github.com/allegro/bigcache"
)

type Cache interface {
	Get(key string) ([]byte, error)
	Set(key string, entry []byte) error
	Delete(key string) error
	Reset() error
	Close() error
}
type CacheWrapToDBHandle struct {
	c       Cache
	innerDb protocol.DBHandle
	logger  protocol.Logger
}

func (c *CacheWrapToDBHandle) GetDbType() string {
	return c.innerDb.GetDbType()
}

func (c *CacheWrapToDBHandle) Get(key []byte) ([]byte, error) {
	skey := string(key)
	//1.get from cache
	v, err := c.c.Get(skey)
	if err == nil && len(v) > 0 {
		return v, nil
	}
	//2.not found in cache, get from inner db
	v, err = c.innerDb.Get(key)
	if err != nil {
		return nil, err
	}
	//3. set value to cache
	err = c.c.Set(skey, v)
	if err != nil {
		c.logger.Warnf("set cache key[%s] get error:%s", skey, err.Error())
	}
	return v, nil
}
func (c *CacheWrapToDBHandle) Put(key []byte, value []byte) error {
	skey := string(key)
	c.put(skey, value)
	return c.innerDb.Put(key, value)
}
func (c *CacheWrapToDBHandle) put(skey string, value []byte) {
	err := c.c.Set(skey, value)
	if err != nil {
		c.logger.Warnf("set cache key[%s] get error:%s", skey, err.Error())
	}
}

func (c *CacheWrapToDBHandle) Has(key []byte) (bool, error) {
	skey := string(key)
	//1.get from cache
	v, err := c.c.Get(skey)
	if err == nil && len(v) > 0 { //has value in cache
		return true, nil
	}
	//query from cache has
	v, err = c.c.Get(cacheHasKey(skey))
	if err == nil && len(v) > 0 { //has value in cache
		return v[0] != 0, nil
	}
	//query from inner db
	has, err := c.innerDb.Has(key)
	if err != nil {
		return has, err
	}
	//set result into cache
	var hasValue byte
	if has {
		hasValue = 1
	}
	err = c.c.Set(cacheHasKey(skey), []byte{hasValue})
	if err != nil {
		c.logger.Warnf("set cache key[%s] get error:%s", skey, err.Error())
	}
	return has, nil
}
func cacheHasKey(key string) string {
	return "#Has#" + key
}
func (c *CacheWrapToDBHandle) Delete(key []byte) error {
	skey := string(key)
	c.delete(skey)
	return c.innerDb.Delete(key)
}
func (c *CacheWrapToDBHandle) delete(skey string) {
	err := c.c.Delete(skey)
	if err != nil && err != bigcache.ErrEntryNotFound {
		c.logger.Warnf("delete cache get error:%s", err)
	}
	err = c.c.Delete(cacheHasKey(skey))
	if err != nil && err != bigcache.ErrEntryNotFound {
		c.logger.Warnf("delete cache get error:%s", err)
	}
}

func (c *CacheWrapToDBHandle) WriteBatch(batch protocol.StoreBatcher, sync bool) error {
	//process cache
	for k, v := range batch.KVs() {
		if len(v) == 0 {
			c.delete(k)
		} else {
			c.put(k, v)
		}
	}
	return c.innerDb.WriteBatch(batch, sync)
}

func (c *CacheWrapToDBHandle) CompactRange(start, limit []byte) error {
	return c.innerDb.CompactRange(start, limit)
}

func (c *CacheWrapToDBHandle) NewIteratorWithRange(start []byte, limit []byte) (protocol.Iterator, error) {
	//Cache一般不支持范围查询和模糊查询，直接从数据库中查
	return c.innerDb.NewIteratorWithRange(start, limit)
}

func (c *CacheWrapToDBHandle) NewIteratorWithPrefix(prefix []byte) (protocol.Iterator, error) {
	//Cache一般不支持范围查询和模糊查询，直接从数据库中查
	return c.innerDb.NewIteratorWithPrefix(prefix)
}

func (c *CacheWrapToDBHandle) GetWriteBatchSize() uint64 {
	//TODO implement me
	panic("implement me GetWriteBatchSize")
}

func (c *CacheWrapToDBHandle) Close() error {
	err := c.c.Close()
	if err != nil {
		c.logger.Warn(err)
	}
	return c.innerDb.Close()
}

func NewCacheWrapToDBHandle(c Cache, innerDb protocol.DBHandle, l protocol.Logger) protocol.DBHandle {
	l.Debug("wrap cache to a db handle")
	return &CacheWrapToDBHandle{
		c:       c,
		innerDb: innerDb,
		logger:  l,
	}
}
