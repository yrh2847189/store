/*
Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.

SPDX-License-Identifier: Apache-2.0
*/

package store

import (
	"encoding/hex"
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"

	"chainmaker.org/chainmaker/common/v2/container"
	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/common/v2/crypto/pkcs11"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/aes"
	"chainmaker.org/chainmaker/common/v2/crypto/sym/sm4"
	"chainmaker.org/chainmaker/common/v2/wal"
	"chainmaker.org/chainmaker/protocol/v2"
	"chainmaker.org/chainmaker/store/v2/binlog"
	"chainmaker.org/chainmaker/store/v2/blockdb"
	"chainmaker.org/chainmaker/store/v2/blockdb/blockfiledb"
	"chainmaker.org/chainmaker/store/v2/blockdb/blockkvdb"
	"chainmaker.org/chainmaker/store/v2/blockdb/blocksqldb"
	"chainmaker.org/chainmaker/store/v2/cache"
	"chainmaker.org/chainmaker/store/v2/conf"
	"chainmaker.org/chainmaker/store/v2/contracteventdb"
	"chainmaker.org/chainmaker/store/v2/contracteventdb/eventsqldb"
	"chainmaker.org/chainmaker/store/v2/dbprovider"
	"chainmaker.org/chainmaker/store/v2/historydb"
	"chainmaker.org/chainmaker/store/v2/historydb/historykvdb"
	"chainmaker.org/chainmaker/store/v2/historydb/historysqldb"
	"chainmaker.org/chainmaker/store/v2/resultdb"
	"chainmaker.org/chainmaker/store/v2/resultdb/resultfiledb"
	"chainmaker.org/chainmaker/store/v2/resultdb/resultkvdb"
	"chainmaker.org/chainmaker/store/v2/resultdb/resultsqldb"
	"chainmaker.org/chainmaker/store/v2/serialization"
	"chainmaker.org/chainmaker/store/v2/statedb"
	"chainmaker.org/chainmaker/store/v2/statedb/statekvdb"
	"chainmaker.org/chainmaker/store/v2/statedb/statesqldb"
	"chainmaker.org/chainmaker/store/v2/test"
	"chainmaker.org/chainmaker/store/v2/txexistdb"
	"chainmaker.org/chainmaker/store/v2/txexistdb/txexistkvdb"
	"github.com/allegro/bigcache"
)

const (
	//StoreBlockDBDir blockdb folder name
	StoreBlockDBDir = "store_block"
	//StoreStateDBDir statedb folder name
	StoreStateDBDir = "store_state"
	//StoreHistoryDBDir historydb folder name
	StoreHistoryDBDir = "store_history"
	//StoreResultDBDir resultdb folder name
	StoreResultDBDir   = "store_result"
	StoreEventLogDBDir = "store_event_log"
	StoreLocalDBDir    = "localdb"
	StoreTxExistDbDir  = "store_txexist"

	DBName_BlockDB   = "blockdb"
	DBName_StateDB   = "statedb"
	DBName_HistoryDB = "historydb"
	DBName_ResultDB  = "resultdb"
	DBName_EventDB   = "eventdb"
	DBName_LocalDB   = "localdb"
	DBName_TxExistDB = "txexistdb"
)

// Factory is a factory function to create an instance of the block store
// which commits block into the ledger.
type Factory struct {
	ioc       *container.Container
	dbFactory *dbprovider.DBFactory
}

func NewFactory() *Factory {
	ioc := container.NewContainer()
	return &Factory{
		ioc:       ioc,
		dbFactory: dbprovider.NewDBFactory(),
	}
}

// NewStore constructs new BlockStore
func (m *Factory) NewStore(chainId string, storeConfig *conf.StorageConfig,
	logger protocol.Logger, p11Handle *pkcs11.P11Handle) (protocol.BlockchainStore, error) {

	dbConfig := storeConfig.BlockDbConfig
	if strings.ToLower(dbConfig.Provider) == "simple" {
		db, err := m.dbFactory.NewKvDB(chainId, conf.DbconfigProviderLeveldb, StoreBlockDBDir,
			dbConfig.LevelDbConfig, logger, nil)

		if err != nil {
			return nil, err
		}

		return test.NewDebugStore(logger, storeConfig, db), nil
	}
	if strings.ToLower(dbConfig.Provider) == "memory" {
		db, err := m.dbFactory.NewKvDB(chainId, conf.DbconfigProviderMemdb, StoreBlockDBDir,
			dbConfig.LevelDbConfig, logger, nil)

		if err != nil {
			return nil, err
		}

		return test.NewDebugStore(logger, storeConfig, db), nil
	}
	m.ioc = container.NewContainer()
	err := m.register(chainId, storeConfig, logger, p11Handle)
	if err != nil {
		return nil, err
	}
	var store protocol.BlockchainStore
	err = m.ioc.Resolve(&store)
	return store, err
}

func (m *Factory) register(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger,
	p11Handle *pkcs11.P11Handle) error {
	//注册logger
	err := m.ioc.Register(func() protocol.Logger { return logger })
	if err != nil {
		return err
	}
	//注册数据落盘对称数据加密对象
	if len(storeConfig.Encryptor) > 0 && len(storeConfig.EncryptKey) > 0 {
		//encryptor, err = buildEncryptor(storeConfig.Encryptor, storeConfig.EncryptKey, p11Handle)
		logger.Debugf("ioc register store encryptor")
		if err = m.ioc.Register(buildEncryptor, container.Parameters(map[int]interface{}{
			0: storeConfig.Encryptor,
			1: storeConfig.EncryptKey,
			2: p11Handle})); err != nil {
			return err
		}
	}

	if err = m.registerFileOrLogDB(chainId, storeConfig, logger); err != nil {
		return err
	}

	//注册StateDB缓存
	if !storeConfig.DisableStateCache {
		if err = m.registerCache(storeConfig.StateCache); err != nil {
			return err
		}
	}
	//注册StateDB缓存
	if !storeConfig.DisableStateCache {
		if err = m.registerCache(storeConfig.StateCache); err != nil {
			return err
		}
	}
	if err = m.registerAllDbHandle(chainId, storeConfig); err != nil {
		return err
	}
	if err = m.registerBusinessDB(chainId, storeConfig); err != nil {
		return err
	}
	var bs protocol.BlockchainStore
	if err = m.ioc.Register(NewBlockStoreImpl, container.Parameters(map[int]interface{}{0: chainId, 1: storeConfig}),
		container.DependsOn(map[int]string{8: DBName_LocalDB}),
		container.Optional(4, 5, 6, 7, 10, 11), //HistoryDB,ResultDB,EventLogDB,TxExistDB,binlog可选
		container.Interface(&bs),
		container.Name("sync_bs")); err != nil {
		return err
	}
	if storeConfig.Async {
		if err = m.ioc.Register(NewAsyncBlockStoreImpl,
			container.DependsOn(map[int]string{0: "sync_bs"}),
			container.Default()); err != nil {
			return err
		}
	}
	return nil
}

func buildEncryptor(encryptor, key string, p11Handle *pkcs11.P11Handle) (crypto.SymmetricKey, error) {
	switch strings.ToLower(encryptor) {
	case "sm4":
		if p11Handle != nil {
			return pkcs11.NewSecretKey(p11Handle, key, crypto.SM4)
		}
		return &sm4.SM4Key{Key: readEncryptKey(key)}, nil
	case "aes":
		if p11Handle != nil {
			return pkcs11.NewSecretKey(p11Handle, key, crypto.AES)
		}
		return &aes.AESKey{Key: readEncryptKey(key)}, nil
	default:
		return nil, errors.New("unsupported encryptor:" + encryptor)
	}
}

func readEncryptKey(key string) []byte {
	reg := regexp.MustCompile("^0[xX][0-9a-fA-F]+$") //is hex
	if reg.Match([]byte(key)) {
		b, _ := hex.DecodeString(key[2:])
		return b
	}
	if key[0] == '/' && pathExists(key) {
		f, err := ioutil.ReadFile(key)
		if err == nil {
			return f
		}
	}
	//对于windows系统，临时文件以C开头
	if key[0] == 'C' && pathExists(key) {
		f, err := ioutil.ReadFile(key)
		if err == nil {
			return f
		}
	}
	return []byte(key)
}
func pathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	return os.IsExist(err)
}

func (m *Factory) registerCache(config *conf.CacheConfig) error {
	if config == nil || len(config.Provider) == 0 || strings.ToLower(config.Provider) == "bigcache" {
		err := m.ioc.Register(newBigCache,
			container.Parameters(map[int]interface{}{0: config}))
		if err != nil {
			return err
		}
	} else {
		panic("cache provider[" + config.Provider + "] not support")
	}
	return nil
}
func newBigCache(cacheConfig *conf.CacheConfig) (cache.Cache, error) {
	bigCacheDefaultConfig := bigcache.Config{
		// number of shards (must be a power of 2)
		Shards: 1024,
		// time after which entry can be evicted
		LifeWindow: 10 * time.Minute,
		// rps * lifeWindow, used only in initial memory allocation
		CleanWindow: 10 * time.Second,
		// max entry size in bytes, used only in initial memory allocation
		MaxEntrySize: 500,
		// prints information about additional memory allocation
		Verbose: true,
		// cache will not allocate more memory than this limit, value in MB
		// if value is reached then the oldest entries can be overridden for the new ones
		// 0 value means no size limit
		HardMaxCacheSize: 128,
		// callback fired when the oldest entry is removed because of its
		// expiration time or no space left for the new entry. Default value is nil which
		// means no callback and it prevents from unwrapping the oldest entry.
		OnRemove: nil,
	}
	if cacheConfig != nil {
		bigCacheDefaultConfig.LifeWindow = cacheConfig.LifeWindow
		bigCacheDefaultConfig.CleanWindow = cacheConfig.CleanWindow
		bigCacheDefaultConfig.MaxEntrySize = cacheConfig.MaxEntrySize
		bigCacheDefaultConfig.HardMaxCacheSize = cacheConfig.HardMaxCacheSize
	}
	return bigcache.NewBigCache(bigCacheDefaultConfig)
}

func (m *Factory) registerDbHandle(chainId string, dbConfig *conf.DbConfig, dbDir, name, dbPrefix string,
	enableCache bool) error {
	if dbConfig.IsKVDB() {
		dbName := dbDir
		if dbConfig.Provider == conf.DbconfigProviderSqlKV {
			dbName = getDbName(dbPrefix, name, chainId)
		}
		config := dbConfig.GetDbConfig()
		var constructor interface{} = m.dbFactory.NewKvDB
		if enableCache { //如果启用了State缓存，则使用WrapCacheToDBHandle来替换StateDB
			constructor = func(chainId, providerName, dbFolder string, config map[string]interface{},
				logger protocol.Logger, encryptor crypto.SymmetricKey, c cache.Cache) (protocol.DBHandle, error) {
				innerDb, err := m.dbFactory.NewKvDB(chainId, providerName, dbFolder, config, logger, encryptor)
				if err != nil {
					return nil, err
				}
				return cache.NewCacheWrapToDBHandle(c, innerDb, logger), nil
			}
		}
		err := m.ioc.Register(constructor,
			container.Parameters(map[int]interface{}{0: chainId, 1: dbConfig.Provider,
				2: dbName, 3: config}),
			container.Name(name),
			container.Optional(5)) //透明数据加密是可选的
		if err != nil {
			return err
		}

	} else {
		dbName := getDbName(dbPrefix, name, chainId)
		err := m.ioc.Register(m.dbFactory.NewSqlDB,
			container.Parameters(map[int]interface{}{0: chainId,
				1: dbName, 2: dbConfig.SqlDbConfig}),
			container.Name(name),
			container.Lifestyle(true))
		if err != nil {
			return err
		}
	}
	return nil
}

func getDbName(dbPrefix, dbName, chainId string) string {
	return dbPrefix + dbName + "_" + chainId
}

func (m *Factory) registerAllDbHandle(chainId string, storeConfig *conf.StorageConfig) error {
	err := m.registerDbHandle(chainId, storeConfig.BlockDbConfig, StoreBlockDBDir,
		DBName_BlockDB, storeConfig.DbPrefix, false)
	if err != nil {
		return err
	}
	err = m.registerDbHandle(chainId, storeConfig.StateDbConfig, StoreStateDBDir,
		DBName_StateDB, storeConfig.DbPrefix, !storeConfig.DisableStateCache)
	if err != nil {
		return err
	}
	if storeConfig.TxExistDbConfig != nil {
		err = m.registerDbHandle(chainId, storeConfig.TxExistDbConfig, StoreTxExistDbDir,
			DBName_TxExistDB, storeConfig.DbPrefix, false)
		if err != nil {
			return err
		}
	}
	if !storeConfig.DisableHistoryDB {
		err = m.registerDbHandle(chainId, &storeConfig.HistoryDbConfig.DbConfig, StoreHistoryDBDir,
			DBName_HistoryDB, storeConfig.DbPrefix, false)
		if err != nil {
			return err
		}
	}
	if !storeConfig.DisableResultDB {
		err = m.registerDbHandle(chainId, storeConfig.ResultDbConfig, StoreResultDBDir,
			DBName_ResultDB, storeConfig.DbPrefix, false)
		if err != nil {
			return err
		}
	}
	if !storeConfig.DisableContractEventDB {
		err = m.registerDbHandle(chainId, storeConfig.ContractEventDbConfig, StoreEventLogDBDir,
			DBName_EventDB, storeConfig.DbPrefix, false)
		if err != nil {
			return err
		}
	}
	return m.registerDbHandle(chainId, storeConfig.GetDefaultDBConfig(), StoreLocalDBDir,
		DBName_LocalDB, storeConfig.DbPrefix, false)
}
func (m *Factory) registerBlockDB(chainId string, storeConfig *conf.StorageConfig) error {
	var (
		err     error
		blockDB blockdb.BlockDB
	)
	dbPrefix := storeConfig.DbPrefix
	if storeConfig.BlockDbConfig.IsKVDB() {
		if storeConfig.DisableBlockFileDb {
			if err = m.ioc.Register(blockkvdb.NewBlockKvDB,
				container.DependsOn(map[int]string{1: DBName_BlockDB}),
				container.Parameters(map[int]interface{}{0: chainId, 3: storeConfig}),
				container.Interface(&blockDB)); err != nil {
				return err
			}
		} else {
			if err = m.ioc.Register(blockfiledb.NewBlockFileDB,
				container.DependsOn(map[int]string{1: DBName_BlockDB}),
				container.Parameters(map[int]interface{}{0: chainId, 3: storeConfig}),
				container.Interface(&blockDB)); err != nil {
				return err
			}
		}
	} else {
		if err = m.ioc.Register(blocksqldb.NewBlockSqlDB,
			container.DependsOn(map[int]string{1: DBName_BlockDB}),
			container.Parameters(map[int]interface{}{0: getDbName(dbPrefix, DBName_BlockDB, chainId)}),
			container.Interface(&blockDB)); err != nil {
			return err
		}
	}
	return nil
}
func (m *Factory) registerStateDB(chainId string, storeConfig *conf.StorageConfig) error {
	var (
		err     error
		stateDB statedb.StateDB
	)
	if storeConfig.StateDbConfig.IsKVDB() {
		if err = m.ioc.Register(statekvdb.NewStateKvDB, container.DependsOn(map[int]string{1: DBName_StateDB}),
			container.Parameters(map[int]interface{}{0: chainId, 3: storeConfig}),
			container.Interface(&stateDB)); err != nil {
			return err
		}
	} else {
		newDbFunc := func(dbName string) (protocol.SqlDBHandle, error) {
			var db protocol.SqlDBHandle
			err = m.ioc.Resolve(&db, container.ResolveName(DBName_StateDB),
				container.Arguments(map[int]interface{}{1: dbName}))
			return db, err
		}
		connPoolSize := 90
		if maxConnSize, ok := storeConfig.StateDbConfig.SqlDbConfig["max_open_conns"]; ok {
			connPoolSize, _ = maxConnSize.(int)
		}
		if err = m.ioc.Register(statesqldb.NewStateSqlDB, container.DependsOn(map[int]string{2: DBName_StateDB}),
			container.Parameters(map[int]interface{}{0: storeConfig.DbPrefix, 1: chainId, 3: newDbFunc, 5: connPoolSize}),
			container.Interface(&stateDB)); err != nil {
			return err
		}
	}
	return nil
}
func (m *Factory) registerHistoryDB(chainId string, storeConfig *conf.StorageConfig) error {
	var (
		err       error
		historyDB historydb.HistoryDB
	)
	if storeConfig.HistoryDbConfig.IsKVDB() {
		if err = m.ioc.Register(historykvdb.NewHistoryKvDB,
			container.DependsOn(map[int]string{2: DBName_HistoryDB}),
			container.Parameters(map[int]interface{}{0: chainId, 1: storeConfig.HistoryDbConfig}),
			container.Interface(&historyDB)); err != nil {
			return err
		}
	} else {
		if err = m.ioc.Register(historysqldb.NewHistorySqlDB,
			container.DependsOn(map[int]string{2: DBName_HistoryDB}),
			container.Parameters(map[int]interface{}{
				0: getDbName(storeConfig.DbPrefix, DBName_HistoryDB, chainId),
				1: storeConfig.HistoryDbConfig}),
			container.Interface(&historyDB)); err != nil {
			return err
		}
	}
	return nil
}
func (m *Factory) registerResultDB(chainId string, storeConfig *conf.StorageConfig) error {
	var (
		err      error
		resultDB resultdb.ResultDB
	)
	if storeConfig.ResultDbConfig.IsKVDB() {
		if storeConfig.DisableBlockFileDb {
			err = m.ioc.Register(resultkvdb.NewResultKvDB, container.DependsOn(map[int]string{1: DBName_ResultDB}),
				container.Parameters(map[int]interface{}{0: chainId, 3: storeConfig}),
				container.Interface(&resultDB))
			if err != nil {
				return err
			}
		} else {
			err = m.ioc.Register(resultfiledb.NewResultFileDB, container.DependsOn(map[int]string{1: DBName_ResultDB}),
				container.Parameters(map[int]interface{}{0: chainId, 3: storeConfig}),
				container.Interface(&resultDB))
			if err != nil {
				return err
			}
		}
	} else {
		if err = m.ioc.Register(resultsqldb.NewResultSqlDB, container.DependsOn(map[int]string{1: DBName_ResultDB}),
			container.Parameters(map[int]interface{}{0: getDbName(storeConfig.DbPrefix, DBName_ResultDB, chainId)}),
			container.Interface(&resultDB)); err != nil {
			return err
		}
	}
	return nil
}

func (m *Factory) registerTxExistDB(chainId string, storeConfig *conf.StorageConfig) error {
	if storeConfig.TxExistDbConfig == nil {
		//如果没有配置TxExistDB，那么就使用BlockDB来判断TxExist
		err := m.ioc.Register(WrapBlockDB2TxExistDB)
		if err != nil {
			return err
		}
	} else if storeConfig.TxExistDbConfig.IsKVDB() {
		var txExistDb txexistdb.TxExistDB
		err := m.ioc.Register(txexistkvdb.NewTxExistKvDB, container.DependsOn(map[int]string{1: DBName_TxExistDB}),
			container.Parameters(map[int]interface{}{0: chainId}),
			container.Interface(&txExistDb))
		if err != nil {
			return err
		}
	}
	return nil
}
func (m *Factory) registerBusinessDB(chainId string, storeConfig *conf.StorageConfig) error {
	if err := m.registerBlockDB(chainId, storeConfig); err != nil {
		return err
	}
	if err := m.registerStateDB(chainId, storeConfig); err != nil {
		return err
	}
	dbPrefix := storeConfig.DbPrefix

	if !storeConfig.DisableHistoryDB {
		if err := m.registerHistoryDB(chainId, storeConfig); err != nil {
			return err
		}
	}
	if !storeConfig.DisableResultDB {
		if err := m.registerResultDB(chainId, storeConfig); err != nil {
			return err
		}
	}
	if !storeConfig.DisableContractEventDB {
		//event db
		var eventDB contracteventdb.ContractEventDB
		err := m.ioc.Register(eventsqldb.NewContractEventDB, container.DependsOn(map[int]string{1: DBName_EventDB}),
			container.Parameters(map[int]interface{}{0: getDbName(dbPrefix, DBName_EventDB, chainId)}),
			container.Interface(&eventDB))
		if err != nil {
			return err
		}
	}
	//register TxExistDB
	return m.registerTxExistDB(chainId, storeConfig)
}

func (m *Factory) registerFileOrLogDB(chainId string, storeConfig *conf.StorageConfig, logger protocol.Logger) error {
	var (
		err    error
		bfdb   *blockfiledb.BlockFile
		walLog *wal.Log
	)
	if !storeConfig.DisableBlockFileDb {
		opts := blockfiledb.DefaultOptions
		opts.NoCopy = true
		opts.NoSync = storeConfig.LogDBSegmentAsync
		if storeConfig.LogDBSegmentSize > 0 { // LogDBSegmentSize default is 20MB
			opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
		}

		bfdbPath := filepath.Join(storeConfig.StorePath, chainId, blockFilePath)
		bfdb, err = blockfiledb.Open(bfdbPath, opts, logger)
		if err != nil {
			panic(fmt.Sprintf("open block file db failed, path:%s, error:%s", bfdbPath, err))
		}
		return m.ioc.Register(func() binlog.BinLogger { return bfdb })
	}

	opts := wal.DefaultOptions
	opts.NoCopy = true
	opts.NoSync = storeConfig.LogDBSegmentAsync
	if storeConfig.LogDBSegmentSize > 0 { // LogDBSegmentSize default is 20MB
		opts.SegmentSize = storeConfig.LogDBSegmentSize * 1024 * 1024
	}

	walPath := filepath.Join(storeConfig.StorePath, chainId, walLogPath)
	walLog, err = wal.Open(walPath, opts)
	if err != nil {
		panic(fmt.Sprintf("open wal log failed, path:%s, error:%s", walPath, err))
	}
	return m.ioc.Register(func() *wal.Log { return walLog })
}

func (m *Factory) NewBlockDB(chainId string, storeConfig *conf.StorageConfig,
	logger protocol.Logger, p11Handle *pkcs11.P11Handle) (blockdb.BlockDB, error) {
	m.ioc = container.NewContainer()
	err := m.register(chainId, storeConfig, logger, p11Handle)
	if err != nil {
		return nil, err
	}
	var db blockdb.BlockDB
	err = m.ioc.Resolve(&db)
	return db, err
}

type noTxExistDB struct {
	db blockdb.BlockDB
}

func (n noTxExistDB) InitGenesis(genesisBlock *serialization.BlockWithSerializedInfo) error {
	return nil
}

func (n noTxExistDB) CommitBlock(blockWithRWSet *serialization.BlockWithSerializedInfo, isCache bool) error {
	return nil
}

func (n noTxExistDB) GetLastSavepoint() (uint64, error) {
	return n.db.GetLastSavepoint()
}

func (n noTxExistDB) TxExists(txId string) (bool, error) {
	return n.db.TxExists(txId)
}

func (n noTxExistDB) Close() {

}

func WrapBlockDB2TxExistDB(db blockdb.BlockDB, log protocol.Logger) txexistdb.TxExistDB {
	log.Info("no TxExistDB config, use BlockDB to replace TxExistDB")
	return &noTxExistDB{db: db}
}
