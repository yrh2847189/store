/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package dbprovider

import (
	"fmt"
	"strings"

	"chainmaker.org/chainmaker/common/v2/crypto"
	"chainmaker.org/chainmaker/protocol/v2"
	badgerdbprovider "chainmaker.org/chainmaker/store-badgerdb/v2"
	leveldbprovider "chainmaker.org/chainmaker/store-leveldb/v2"
	rawsqlprovider "chainmaker.org/chainmaker/store-sqldb/v2"
	tikvdbprovider "chainmaker.org/chainmaker/store-tikv/v2"
	"chainmaker.org/chainmaker/store/v2/conf"
	"github.com/mitchellh/mapstructure"
)

func NewDBFactory() *DBFactory {
	return &DBFactory{}
}

type DBFactory struct {
	//ioc container.Container
}

func (f *DBFactory) NewKvDB(chainId, providerName, dbFolder string, config map[string]interface{},
	logger protocol.Logger, encryptor crypto.SymmetricKey) (protocol.DBHandle, error) {
	providerName = strings.ToLower(providerName)
	switch providerName {
	case conf.DbconfigProviderLeveldb:
		dbConfig := &leveldbprovider.LevelDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &leveldbprovider.NewLevelDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return leveldbprovider.NewLevelDBHandle(input), nil
	case conf.DbconfigProviderBadgerdb:
		dbConfig := &badgerdbprovider.BadgerDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &badgerdbprovider.NewBadgerDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbFolder:  dbFolder,
		}
		return badgerdbprovider.NewBadgerDBHandle(input), nil
	case conf.DbconfigProviderTikvdb:
		dbConfig := &tikvdbprovider.TiKVDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &tikvdbprovider.NewTikvDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return tikvdbprovider.NewTiKVDBHandle(input), nil
	case conf.DbconfigProviderSqlKV:
		dbConfig := &rawsqlprovider.SqlDbConfig{}
		err := mapstructure.Decode(config, dbConfig)
		if err != nil {
			return nil, err
		}
		input := &rawsqlprovider.NewSqlDBOptions{
			Config:    dbConfig,
			Logger:    logger,
			Encryptor: encryptor,
			ChainId:   chainId,
			DbName:    dbFolder,
		}
		return rawsqlprovider.NewKVDBHandle(input), nil
	case conf.DbconfigProviderMemdb:
		return leveldbprovider.NewMemdbHandle(), nil
	default:
		return nil, fmt.Errorf("unsupported provider:%s", providerName)
	}
}

func (f *DBFactory) NewSqlDB(chainId, dbName string, config map[string]interface{},
	logger protocol.Logger) (
	protocol.SqlDBHandle, error) {
	dbConfig := &rawsqlprovider.SqlDbConfig{}
	err := mapstructure.Decode(config, dbConfig)
	if err != nil {
		return nil, err
	}
	input := &rawsqlprovider.NewSqlDBOptions{
		Config:    dbConfig,
		Logger:    logger,
		Encryptor: nil,
		ChainId:   chainId,
		DbName:    dbName,
	}
	logger.Debugf("initial new sql db for %s", dbName)
	return rawsqlprovider.NewSqlDBHandle(input), nil
}
