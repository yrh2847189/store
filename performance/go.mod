module chainmaker.org/chainmaker/store/performance

go 1.15

require (
	chainmaker.org/chainmaker/localconf/v2 v2.1.1-0.20211214124610-bb7620382194
	chainmaker.org/chainmaker/logger/v2 v2.1.1-0.20211214124250-621f11b35ab0
	chainmaker.org/chainmaker/pb-go/v2 v2.1.1-0.20220119062501-d1b3a201f7fb
	chainmaker.org/chainmaker/protocol/v2 v2.1.2-0.20220121132530-b83d04e6f3cc
	chainmaker.org/chainmaker/store/v2 v2.1.1
	chainmaker.org/chainmaker/utils/v2 v2.1.1-0.20220114120415-8e1af1e262a7
	github.com/spf13/cobra v1.1.1
)

replace (
	chainmaker.org/chainmaker/store/v2 => ../
	google.golang.org/grpc => google.golang.org/grpc v1.26.0
)
