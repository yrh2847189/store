VERSION=v2.2.0
QC=v2.2.0
gomod:
	go get chainmaker.org/chainmaker/common/v2@$(VERSION)
	go get chainmaker.org/chainmaker/pb-go/v2@$(QC)
	go get chainmaker.org/chainmaker/protocol/v2@$(QC)
	go get chainmaker.org/chainmaker/store-badgerdb/v2@$(VERSION)
	go get chainmaker.org/chainmaker/store-leveldb/v2@$(VERSION)
	go get chainmaker.org/chainmaker/store-sqldb/v2@$(QC)
	go get chainmaker.org/chainmaker/store-tikv/v2@$(VERSION)
	go get chainmaker.org/chainmaker/utils/v2@$(QC)
	go mod tidy
ut:
	go test ./...
	(gocloc --include-lang=Go --output-type=json . | jq '(.total.comment-.total.files*6)/(.total.code+.total.comment)*100')